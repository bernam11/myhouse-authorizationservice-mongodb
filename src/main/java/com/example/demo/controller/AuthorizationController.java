package com.example.demo.controller;

import com.example.demo.model.Authorization;
import com.example.demo.service.AuthorizationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/authorizations")
@Slf4j
public class AuthorizationController {

    @Autowired
    AuthorizationService authorizationService;


    @Operation(summary = "GET all authorizations")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the authorizations",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Authorization.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "No Auhtorizations found",
                    content = @Content) })
    @GetMapping("")
    public List<Authorization> listAuthorizations()
    {
        try{
            log.info("AuthorizationsController: listAuhtorizations");
            return authorizationService.listAuthorizations();
        }catch (Exception e){
            log.error("MAME TU ERROR", e);
            throw e;
        }

    }
    @Operation(summary = "GET authorizations by serviceID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the authorizations",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Authorization.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Auhtorizations not found",
                    content = @Content) })
    @GetMapping("/service/{serviceId}")
    public List<Authorization> listAuthorizationsWIthServiceId(@PathVariable("serviceId") String serviceId)

    {
        log.info("AuthorizationsController: listAuthorizationsWIthServiceId");
        return authorizationService.listAuthorizationsWithServiceId(serviceId);
    }

    @Operation(summary = "GET authorization by userID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the authorizations",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Authorization.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Auhtorizations not found",
                    content = @Content) })
    @GetMapping("/users/{userId}")
    public List<Authorization> listAuthorizationsWIthUserId(@PathVariable("serviceId") String userId)

    {
        log.info("AuthorizationsController: listAuthorizationsWIthUserId");
        return authorizationService.listAuthorizationsWithUserId(userId);
    }


    @Operation(summary = "GET authorization by it's ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the authorization",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Authorization.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Auhtorization not found",
                    content = @Content) })
    @GetMapping("/{id}")
    public Authorization getAuthorizationWithId(@PathVariable("id") String id)
    {
        log.info("AuthorizationConntroller: getAuhtorizationWithId");
        return authorizationService.getAuthoriaztionById(id);
    }





    @Operation(summary = "UPSERT Authorization by Authorization")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Authorizations OK",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Authorization.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Auhtorization not found",
                    content = @Content) })
    @PutMapping("")
    public Authorization upsertAuthorization(@RequestBody Authorization authorization)
    {
        log.info("AuthorizationController: upsertAuthorization");
        return authorizationService.upsertAuthorization(authorization);
    }


    @Operation(summary = "UPSERT Authorization by ID and Authorization Level")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Authorizations OK",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Authorization.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Auhtorization not found",
                    content = @Content) })
    @PutMapping("/{id}/{authorizationLevel}")
    public Authorization upsertAuthorizationLevel(@PathVariable("id") String id, @PathVariable("authorizationLevel") int authorizationLevel)
    {
        try {
            log.info("ContactsController: upsertAuthorizationLevel");
            return authorizationService.upsertAuthorizationLevel(id, authorizationLevel);
        }catch (Exception e){
            log.error("MAME ERROR ZASA", e);
            throw e;
        }

    }




    @Operation(summary = "DELETE Authorization by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Authorizations DELETED",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Authorization.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Auhtorization not found",
                    content = @Content) })
    @DeleteMapping("/{id}")
    public Authorization deleteAuthorizationById(@PathVariable("id") String id)
    {
        log.info("AuthorizationController: deleteAuthorizationById");
        return authorizationService.deleteAuthorizationById(id);
    }


    @Operation(summary = "DELETE Authorization by Authorization")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Authorizations DELETED",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Authorization.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Auhtorization not found",
                    content = @Content) })
    @DeleteMapping("")
    public Authorization deleteAuthorization(@RequestBody Authorization authorization)
    {
        log.info("AuthorizationController: deleteAuthorization");
        return authorizationService.deleteAuthorization(authorization);
    }


}
