package com.example.demo.service;


import com.example.demo.model.Authorization;
import com.example.demo.model.AuthorizationLevel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
@Slf4j

public class AuthorizationService {
    @Autowired
    private  AuthorizationDatabase authorizationDatabase;

    private static final String ID = "id";
    private static final String USER = "userId";
    private static final String SERVICE = "serviceID";
    private static final String LEVEL = "authorizationLevel";
    enum Level {
        ADMIN,
        READONLY,
        BLOCKED
    }


    private static boolean checkRegex(String pattern, String value)
    {
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(value);
        return m.find();
    }

    private static boolean checkRegex(String pattern, List<String> values)
    {
        Pattern p = Pattern.compile(pattern);

        for (String v : values)
        {
            if (p.matcher(v).find()) return true;
        }

        return false;
    }

    /*
     * returns true if the contact conforms the query, false otherwise
     *
     */
    private static boolean authorizationSelector(Authorization contact, Map<String, String> query)
    {
        // check id ("userId_service_id")
        if (query.containsKey(ID) && !query.get(ID).equals(contact.getId()))
        {
            return false;
        }

        // check userId ("login_password")
        if (query.containsKey(USER) && !query.get(USER).equals(contact.getUser_id()))
        {
            return false;
        }

        // check serviceId ("serviceId")
        if (query.containsKey(SERVICE) && !query.get(SERVICE).equals(contact.getService_id()))
        {
            return false;
        }

        // check authorizationLevel   LEVEL INPUT 1 = BLOCK   2 = READONLY   3 =  ADMIN
        // if (query.containsKey(LEVEL) &&  !(Level.values()[Integer.parseInt(query.get(LEVEL))-1].equals(contact.getAuthorizationLevel()))  ) { return false; }
        if (query.containsKey(LEVEL)) {
            Integer.parseInt(query.get(LEVEL));
            return false;
        }

        return true;
    }

    private static String getNewId(Authorization authorizations)
    {
        String id = authorizations.getUser_id() +"_"+ authorizations.getService_id();

        return id;
    }


    public List<Authorization> listAuthorizations()
    {
        log.info("ContactsService - listContacts");

        List<Authorization> authorizations =  authorizationDatabase.getAuthorizations();

        return authorizations;
    }

    public List<Authorization> listAuthorizationsWithServiceId(String serviceId)
    {
        log.info("AuhtorizationService - listAuhtorizationsWithServiceId, id: " + serviceId);

        List<Authorization> authorizations = authorizationDatabase.getAuthorizations();
        return authorizations.stream().filter(c -> c.getService_id().contains(serviceId)).collect(Collectors.toList());
    }

    public List<Authorization> listAuthorizationsWithUserId(String userId)
    {
        log.info("AuhtorizationService - listAuhtorizationsWithUserid, id: " + userId);

        List<Authorization> contacts = authorizationDatabase.getAuthorizations();
        return contacts.stream().filter(c -> c.getUser_id().contains(userId)).collect(Collectors.toList());
    }



    public Authorization getAuthoriaztionById(String id)
    {
        log.info("AuthorizatonService - getAuthorizatonById, id: " + id);

        List<Authorization> authorizations = authorizationDatabase.getAuthorizations();
        Optional<Authorization> authorization = authorizations.stream().filter(c -> c.getId().compareTo(id) == 0).findFirst();
        return authorization.orElse(null);
    }


    public Authorization upsertAuthorizationLevel(String id, int level)
    {
        log.info("AuthorizationService - updateLevel, id: " + id + " level: " + level);

        List<Authorization> authorizations = authorizationDatabase.getAuthorizations();

        // FIRST ADD
        if (authorizations == null){
            Authorization authorization = new Authorization();
            authorization.setId(id);
            String[]data = id.split("_");
            authorization.setUser_id((data.length == 2) ? data[0] : data[0]+"_"+data[1]);
            authorization.setService_id((data.length == 2) ? data[1] : data[2]);
            authorization.setAuthorizationLevel(AuthorizationLevel.values()[level-1]);
            authorizationDatabase.setAuthorizations(Arrays.asList(authorization));
            return authorization;
        }
        else{
            Authorization authorization = getAuthoriaztionById(id);
            if (authorization == null){
                authorization = new Authorization();
                authorization.setId(id);
                String[]data = id.split("_");
                authorization.setUser_id((data.length == 2) ? data[0] : data[0]+"_"+data[1]);
                authorization.setService_id((data.length == 2) ? data[1] : data[2]);
                authorizations.add(authorization);
            }
            authorization.setAuthorizationLevel(AuthorizationLevel.values()[level-1]);

            authorizationDatabase.setAuthorizations(authorizations);
            return authorization;
        }
    }


    public Authorization upsertAuthorization(Authorization authorization_new)
    {
        log.info("AuthorizationService - upsertAuthorization");

        List<Authorization> authorizations = authorizationDatabase.getAuthorizations();
        String id = getNewId(authorization_new);


        Authorization authorization = getAuthoriaztionById(id);

        if (authorization == null){
            authorization_new.setUser_id(id);
            authorizations.add(authorization_new);
        }
        else{
            authorization.setAuthorizationLevel(authorization_new.getAuthorizationLevel());
            authorizations.add(authorization);
        }
        authorizationDatabase.setAuthorizations(authorizations);

        return authorization;
    }



    public Authorization deleteAuthorizationById(String id)
    {
        log.info("AuthorizationService - deleteAuthorization, id: " + id);

        List<Authorization> authorizations = authorizationDatabase.getAuthorizations();

        Authorization authorization = getAuthoriaztionById(id);
        authorizations.remove(authorization);

        authorizationDatabase.setAuthorizations(authorizations);

        return authorization;
    }

    public Authorization deleteAuthorization(Authorization authorization)
    {
        log.info("AuthorizationService - deleteAuthorization  ");

        List<Authorization> authorizations = authorizationDatabase.getAuthorizations();

        String id = getNewId(authorization);

        authorization.setId(id);

        authorizations.remove(authorization);

        authorizationDatabase.setAuthorizations(authorizations);

        return authorization;
    }







}
