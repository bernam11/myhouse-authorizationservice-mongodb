package com.example.demo.service;


import com.example.demo.model.Authorization;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.stream.Collectors;

@Service
@Slf4j
public class AuthorizationDatabase {
    private static final Semaphore mutex = new Semaphore(1);
    private static final String location = "authorization.json";

    private static final ObjectMapper mapper;

    static
    {
        mapper = new ObjectMapper();
    }

    public List<Authorization> getAuthorizations()
    {
        List<Authorization>  authorizations = new ArrayList<>();

        try
        {
            mutex.acquire();

            authorizations = Arrays.stream(mapper.readValue(new FileReader(location), Authorization[].class)).collect(Collectors.toList());
        }
        catch (InterruptedException | IOException exception)
        {
            if (authorizations == null){
                return null;
            }
            else{
                log.error("Error during getContacts: ", exception);

            }
        }
        finally
        {
            mutex.release();
        }
        return authorizations;
    }

    public boolean setAuthorizations(List<Authorization> authorizations)
    {
        boolean result = true;
        try
        {
            mutex.acquire();
            mapper.writeValue(new FileWriter(location), authorizations);
        }
        catch (IOException | InterruptedException exception)
        {
            log.error("Error during setContacts: ", exception);

            result = false;
        }
        finally
        {
            mutex.release();
        }
        return result;
    }


}
