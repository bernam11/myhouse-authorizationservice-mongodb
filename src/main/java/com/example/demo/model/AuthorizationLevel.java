package com.example.demo.model;

public enum AuthorizationLevel {
    BLOCKED,
    READONLY,
    ADMIN
}
