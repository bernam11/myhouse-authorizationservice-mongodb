package com.example.demo.model;


import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode
@Data
public class Authorization {



    private  String id;
    private  String user_id;
    private  String service_id;
    private AuthorizationLevel authorizationLevel;
}
