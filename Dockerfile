FROM maven:latest as builder

ADD . /usr/src

WORKDIR /usr/src
RUN mvn package
#
#
#FROM openjdk:11-jre-slim-buster as app
#
#COPY --from=builder /usr/src/target/myhouse-authorizationService-0.0.1-SNAPSHOT.jar /usr/src/app/
#
#WORKDIR /usr/src/app
#
#CMD java -jar myhouse-authorizationService-0.0.1-SNAPSHOT.jar


FROM maven:latest as builder
ENV APP_HOME=/root/dev/app/
RUN mkdir -p $APP_HOME/src
WORKDIR $APP_HOME
COPY ./src $APP_HOME/src
COPY pom.xml $APP_HOME/pom.xml
RUN mvn package

FROM openjdk:11-jre-slim-buster
WORKDIR /usr/app/
COPY --from=builder /root/dev/app/target/myhouse-authorizationService-0.0.1-SNAPSHOT-jar-with-dependencies.jar .
EXPOSE 9002
CMD ["java", "-jar", "/usr/app/myhouse-authorizationService-0.0.1-SNAPSHOT-jar-with-dependencies.jar", "--spring.config.location=/usr/app/config.properties"]

#FROM openjdk:8-jdk-alpine
#ARG JAR_FILE=target/*.jar
#COPY ${JAR_FILE} app.jar
#ENTRYPOINT ["java","-jar","/app.jar"]


#
##FROM maven:3-jdk-8-alpine
##
##WORKDIR /usr/src/app
##
##COPY . /usr/src/app
##RUN mvn package
##
##ENV PORT 5000
##EXPOSE $PORT
##CMD [ "sh", "-c", "mvn -Dserver.port=${PORT} spring-boot:run" ]

